import CartParser from './CartParser';
import { readFileSync } from 'fs';
import uuid from 'uuid';

jest.mock('fs');
jest.mock('uuid');

/**
 * @type {CartParser}
 */
let parser;

beforeAll(() => {
  uuid.v4.mockReturnValue('unique_id');
});

beforeEach(() => {
  parser = new CartParser();
});

describe('CartParser - unit tests', () => {
  let expectedDataFromFile;

  beforeEach(() => {
    expectedDataFromFile = 'Product name,Price,Quantity\n' + 'Mollis consequat,9.00,2';
    readFileSync.mockReturnValue(expectedDataFromFile);
  });

  afterEach(() => {
    readFileSync.mockRestore();
  });

  describe('readFile', () => {
    it('Reads from file', () => {
      const result = parser.readFile('./some-data.csv');

      expect(result).toBe(expectedDataFromFile);
    });
  });

  describe('parseLine', () => {
    it('Parses CSV line', () => {
      const csvLine = 'Mollis consequat,9.00,2';
      const expectedResult = {
        id: uuid.v4(),
        name: 'Mollis consequat',
        price: 9,
        quantity: 2,
      };

      const parsedLine = parser.parseLine(csvLine);

      expect(parsedLine).toEqual(expectedResult);
    });
    it('Ignores values those are not needed', () => {
      const csvLine = 'Mollis consequat,9.00,2,10,some value';
      const expectedResult = {
        id: uuid.v4(),
        name: 'Mollis consequat',
        price: 9,
        quantity: 2,
      };

      const parsedLine = parser.parseLine(csvLine);

      expect(parsedLine).toEqual(expectedResult);
    });
  });

  describe('validate', () => {
    it('Validates with no errors when proper contents given', () => {
      const contents =
        'Product name,Price,Quantity\n' +
        'Consectetur adipiscing,28.72,10\n' +
        'Condimentum aliquet,13.90,1';

      const errors = parser.validate(contents);

      expect(errors).toHaveLength(0);
    });

    it('Fails validation if only header provided', () => {
      const contents = 'Product name,Price,Quantity\n';

      const errors = parser.validate(contents);

      expect(errors).toHaveLength(0);
    });

    it('Fails validation with errors in header', () => {
      const contents =
        'Product name,Cost,Amount\n' +
        'Consectetur adipiscing,28.72,10\n' +
        'Condimentum aliquet,13.90,1';

      const errors = parser.validate(contents);

      expect(errors).toHaveLength(2);
      expect(errors[0].type).toBe(parser.ErrorType.HEADER);
      expect(errors[1].column).toBe(2);
      expect(errors[1].row).toBe(0);
    });

    it('Fails validation with errors in values', () => {
      const contents =
        'Product name,Price,Quantity\n' +
        'Consectetur adipiscing,28.72,ten\n' +
        'Condimentum aliquet,-13.90,1\n' +
        ',18.90,1';

      const errors = parser.validate(contents);

      expect(errors).toHaveLength(3);

      expect(errors[0].type).toBe(parser.ErrorType.CELL);
      expect(errors[1].type).toBe(parser.ErrorType.CELL);
      expect(errors[2].type).toBe(parser.ErrorType.CELL);

      expect(errors[0].message).not.toBe(errors[1].message);
      expect(errors[0].message).not.toBe(errors[2].message);

      expect(errors[0].column).toBe(2);
      expect(errors[2].row).toBe(3);
    });

    it('Fails validation when lack amount of values in a row', () => {
      const contents =
        'Product name,Price,Quantity\n' + 'Consectetur adipiscing,10\n' + '28.72,10';

      const errors = parser.validate(contents);

      expect(errors).toHaveLength(2);

      expect(errors[0].type).toBe(parser.ErrorType.ROW);
      expect(errors[1].type).toBe(parser.ErrorType.ROW);

      expect(errors[0].column).toBe(-1);
      expect(errors[0].message).toBe(errors[1].message);
    });

    it('Ignores empty lines', () => {
      const contents =
        'Product name,Price,Quantity\n' +
        '\n' +
        'Consectetur adipiscing,28.72,10\n' +
        '\n' +
        '\n' +
        'Condimentum aliquet,13.90,1';

      const errors = parser.validate(contents);

      expect(errors).toHaveLength(0);
    });
  });

  describe('calcTotal', () => {
    it('Calculates cart total', () => {
      const cartItems = [
        {
          price: 2.2,
          quantity: 3,
        },
        {
          price: 3.4,
          quantity: 1,
        },
      ];

      const total = parser.calcTotal(cartItems);

      expect(total).toBe(10);
    });
  });

  describe('parse', () => {
    it('Throws an error if data given is bad', () => {
      readFileSync.mockReturnValue(
        'Product name,Price\n' + 'Mollis consequat,nine bucks,2'
      );

      expect(() => parser.parse('it-throws.csv')).toThrowError('Validation failed!');
    });
  });
});

describe('CartParser - integration test', () => {
  beforeAll(() => {
    const csv =
      'Product name,Price,Quantity\n' +
      'Mollis consequat,9.00,2\n' +
      'Tvoluptatem,10.32,1\n' +
      'Scelerisque lacinia,18.90,1\n' +
      'Consectetur adipiscing,28.72,10\n' +
      'Condimentum aliquet,13.90,1';
    readFileSync.mockReturnValue(csv);
  });

  afterAll(() => {
    readFileSync.mockRestore();
  });

  test('Successfully parses CSV files', () => {
    const expected = {
      items: [
        {
          id: uuid.v4(),
          name: 'Mollis consequat',
          price: 9,
          quantity: 2,
        },
        {
          id: uuid.v4(),
          name: 'Tvoluptatem',
          price: 10.32,
          quantity: 1,
        },
        {
          id: uuid.v4(),
          name: 'Scelerisque lacinia',
          price: 18.9,
          quantity: 1,
        },
        {
          id: uuid.v4(),
          name: 'Consectetur adipiscing',
          price: 28.72,
          quantity: 10,
        },
        {
          id: uuid.v4(),
          name: 'Condimentum aliquet',
          price: 13.9,
          quantity: 1,
        },
      ],
      total: 348.32,
    };

    const result = parser.parse('cart.csv');

    expect(result).toEqual(expected);
  });
});
